﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crypto_Toolkit
{
    class Interface
    {
        private string[] optionNames = { "Randomness", "AES", "RSA" };
        public int option;

        public Interface()
        {
            int lenArray(string[] arr)
            {
                int res = arr.Length;
                foreach (string item in arr)
                {
                    if (String.IsNullOrEmpty(item))
                    {
                        res -= 1;
                    }
                }
                return res;
            }

            Console.WriteLine("------------------------------");

            for (int i = 0; i < lenArray(optionNames); i ++)
            {
                string constructed = "";
                int remaining;
                constructed = constructed + "| ~" + optionNames[i] + " = " + (i + 1).ToString();
                remaining = 30 - constructed.Length - 1;
                Console.Write(constructed);
                for (int j = 0; j < remaining; j++)
                {
                    Console.Write(" ");
                }

                Console.WriteLine("|");
            }

            Console.WriteLine("------------------------------");
            Console.WriteLine();
            
            try
            {
                Console.Write("Please enter your selection: ");
                option = Int32.Parse(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Error! Please try again...");
                option = 0;
            }
        }
    }
}
