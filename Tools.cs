﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Crypto_Toolkit
{
    class Tools
    {
        public class Random
        {
            public Double RandomDouble;
            public int RandomInt;
            public String RandomString;

            private int StringSize = 25;

            public Random()
            {
                RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();

                // Set "RandomDouble"
                var doubleByte = new byte[8];
                provider.GetBytes(doubleByte);
                RandomDouble = BitConverter.ToDouble(doubleByte, 0);

                // Set "RandomInt"
                var intByte = new byte[4];
                provider.GetBytes(intByte);
                RandomInt = BitConverter.ToInt32(intByte, 0);

                // Set "RandomString"
                char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
                var stringByte = new byte[4 * StringSize];
                provider.GetBytes(stringByte);
                StringBuilder result = new StringBuilder(StringSize);
                for (int i = 0; i < StringSize; i++)
                {
                    var rnd = BitConverter.ToUInt32(stringByte, i * 4);
                    var idx = rnd % chars.Length;

                    result.Append(chars[idx]);
                }
                RandomString = result.ToString();
            }
        }
    }
}